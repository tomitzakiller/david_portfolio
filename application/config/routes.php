<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['david/(:any)'] = 'welcome/david/$1';
$route['matematika/(:any)/(:any)'] = 'welcome/sabiranje/$1/$2';
$route['glavna'] = 'welcome';
$route['translate_uri_dashes'] = FALSE;
$route['api/get_text'] = 'api/get_text';
$route['api/get_json'] = 'api/get_json';
$route['api/api_post'] = 'api/api_post';
$route['api/saberi'] = 'api/saberi';
$route['api/matematika'] = 'api/matematika';
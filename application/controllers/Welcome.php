<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	private $meta = [
		'page' => 'stranica/glavna'
	];


	public function index()
	{
		$data['ime'] = "David";
		$this->load->view('index', $this->meta);
	}
	public function david($opis) {
		$this->load->view("david");

	}
	public function sabiranje($broj1, $broj2){
		$this->load->model("matematika");
		$rezultat = $this->matematika->saberi($broj1, $broj2);
		echo $rezultat;
	}
}

<?php
    $this->load->helper('url');
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="<?php echo base_url('public/css/'.$page.'.css')?>"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo base_url('public/js/script.js')?>"></script>
</head>
<body>
    <?php $this->load->view("stranica/header"); ?>
    <?php $this->load->view($page); ?>
</body>
</html>
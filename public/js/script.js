$(document).ready(function(){

    $.ajax({
        url: "https://reqres.in/api/users?page=2",
        method: "GET",
        success: function(podaci) {
            console.log(podaci["page"]);
            console.log(podaci["data"][2]["first_name"]);
            for(podatak in podaci["data"]){
                console.log(podaci["data"][podatak]);
                console.log(podaci["data"][podatak]["id"])
            }
                
        }
    });

    $.ajax({
        url: "api/get_text",
        method: "GET",
        success: function(data){
            console.log(data);
        }
    });
    $.ajax({
        url: "api/get_json",
        method: "GET",
        success: function(data){
            console.log(data);
        }
    });
    $.ajax({
        url: "https://reqres.in/api/users",
        method: "POST",
        data: {"name": "morpheus","job": "leader"},
        success: function(info){
            console.log(info);
        }
    });
    $.ajax({
        url: "api/api_post",
        method: "POST",
        data: {"name": "morpheus", "password": "123"},
        success: function(info){
            console.log(info);
        }
    });
    $.ajax({
        url: "api/saberi",
        method: "POST",
        data: {"jedan": 1, "dva": 2},
        success: function(info){
            console.log(info);
        }
    })
    $.ajax({
        url: "api/matematika",
        method: "POST",
        data: {"broj1": 3, "broj2": 0, "znak" : "/"},
        success: function(info){
            console.log(info);
        }
    })
});